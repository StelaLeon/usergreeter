enablePlugins(sbtdocker.DockerPlugin, JavaAppPackaging)

name := "new-user-greeter"

version := "1.0"

scalaVersion := "2.13.1"

lazy val akkaVersion    = "2.6.10"
val AkkaHttpVersion     = "10.2.1"
val AlpakkaKafkaVersion = "2.0.5"
val circeVersion        = "0.13.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed"         % akkaVersion,
  "ch.qos.logback"     % "logback-classic"          % "1.2.3",
  "com.typesafe.akka" %% "akka-stream"              % akkaVersion,
  "com.typesafe.akka" %% "akka-http"                % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream-kafka"        % AlpakkaKafkaVersion,
  "io.circe"          %% "circe-parser"             % circeVersion,
  "io.circe"          %% "circe-core"               % circeVersion,
  "io.circe"          %% "circe-generic"            % circeVersion,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion     % Test,
  "com.typesafe.akka" %% "akka-http-testkit"        % AkkaHttpVersion % Test,
  "com.typesafe.akka" %% "akka-testkit"             % akkaVersion     % Test,
  "com.typesafe.akka" %% "akka-stream-testkit"      % akkaVersion     % Test,
  "org.scalatest"     %% "scalatest"                % "3.1.0"         % Test
)

buildOptions in docker := BuildOptions(
  cache = false,
  removeIntermediateContainers = BuildOptions.Remove.Always,
  pullBaseImage = BuildOptions.Pull.Always
)

dockerExposedPorts := Seq(8080)
dockerExposedVolumes := Seq("/opt/docker/logs")
