package me.greeter.challenge

import akka.actor.typed.ActorSystem
import akka.kafka.{CommitterSettings, ConsumerSettings, Subscriptions}
import akka.kafka.scaladsl.{Committer, Consumer}
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import me.greeter.challenge.models.{Greeting, NewJoiner}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

import scala.concurrent.ExecutionContextExecutor

trait KafkaStreamPipeline extends PostGreeter {
  // this was made/written as an option read from kafka.
  private def startGreetingPipeline(implicit
      system: ActorSystem[_],
      materializer: Materializer,
      ec: ExecutionContextExecutor
  ) = {
    val committerSettings = CommitterSettings(system)

    val consumerSettings =
      ConsumerSettings(system, Some(new StringDeserializer), Some(new StringDeserializer))
        .withBootstrapServers("localhost:9092")
        .withGroupId("group12")
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

    println("Starting server on.... ")
    Consumer
      .committableSource(consumerSettings, Subscriptions.topics("readerTopic"))
      .mapAsync(10) { msg =>
        processMessage(msg.record.value).map(_ => msg.committableOffset)
      }
      .via(Committer.flow(committerSettings.withMaxBatch(1)))
      .toMat(Sink.seq)(DrainingControl.apply)
      .run()
  }

  def processMessage(
      value: String
  )(implicit system: ActorSystem[_], materializer: Materializer, ec: ExecutionContextExecutor) = {
    println(s"value: ${value}")
    val newJoiner = NewJoiner.getNewJoiner(value).get

    //@todo do the user gatherings

    val greeting = new Greeting(
      sender = "your@email.com",
      receiver = newJoiner.id,
      message = s"Hi ${newJoiner.name}, welcome to komoot. Lise, Anna and Stephen also joined recently",
      recent_user_ids = Array(5, 6)
    )

    executePostGreeting(greeting = greeting)
  }

}
