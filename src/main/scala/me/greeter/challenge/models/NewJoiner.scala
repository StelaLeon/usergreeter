package me.greeter.challenge.models

import java.time.LocalDateTime

import io.circe._
import io.circe.generic.semiauto._
import io.circe.syntax.EncoderOps
import org.slf4j.LoggerFactory

object NewJoiner {
  import io.circe.parser.parse
  val logger = LoggerFactory.getLogger("ServiceRoutes")

  implicit val newJoinerDecoder: Decoder[NewJoiner] = (c: HCursor) =>
    for {
      name      <- c.downField("name").as[String]
      id        <- c.downField("id").as[Int] //.map(id => UUID.fromString(id))
      createdAt <- c.downField("created_at").as[String].map(LocalDateTime.parse)
    } yield (NewJoiner.apply(name, id, createdAt))

  def getNewJoiner(newJoiner: String): Option[NewJoiner] = {
    parse(newJoiner) match {
      case Left(failure) => {
        logger.error(
          s"Invalid JSON :( with failure: ${failure.message}"
        )
        None
      }
      case Right(json) =>
        json.as[NewJoiner] match {
          case Right(value) => {
            Some(value)
          }
          case Left(err) => {
            logger.error(s"Error: ${err.message}")
            None
          }
        }
    }
  }
}
case class NewJoiner(name: String, id: Int, createdAt: LocalDateTime)

object Greeting {
  implicit val greetingEncoder: Encoder[Greeting] = deriveEncoder
  def getGreetingString(greeting: Greeting): String = {
    greeting.asJson.asString.getOrElse("")
  }
}
case class Greeting(sender: String, receiver: Int, message: String, recent_user_ids: Array[Int])
