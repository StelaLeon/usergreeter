package me.greeter.challenge

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{complete, get, path, post, _}
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import io.circe.parser.parse
import me.greeter.challenge.NewJoinersRegistry.{ActionPerformed, GetNewJoiners, NewUserJoined}
import me.greeter.challenge.models.NewJoiner
import org.slf4j.LoggerFactory

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

class ServiceRoutes(userRegistry: ActorRef[NewJoinersRegistry.Command])(implicit val system: ActorSystem[_]) extends SnsSubscriber {

  this: PostGreeter =>

  // If ask takes more time than this to complete the request is failed
  private implicit val timeout              = Timeout(2.seconds)
  private implicit val ex: ExecutionContext = system.executionContext
  override val logger                       = system.log

  def getUsers(): Future[Users] =
    userRegistry.ask(GetNewJoiners)
  def createUser(user: NewJoiner): Future[ActionPerformed] =
    userRegistry.ask(NewUserJoined(user, _))

  val userRoutes: Route =
    path("notify") {
      get {
        complete(getUsers().map(_.users.take(5).map(_.toString).mkString(" \n ")))
      } ~
        post {
          //https://docs.aws.amazon.com/sns/latest/dg/SendMessageToHttp.prepare.html
          headerValueByName("x-amz-sns-message-type") { snsHeaderType =>
            entity(as[String]) { bodyStr =>
              snsHeaderType match {
                case "SubscriptionConfirmation" => {
                  val subscr = getObjectMessage[SubscriptionConfirmation](bodyStr)
                  logger.debug(s"Received ${snsHeaderType} with message id: ${subscr.get.MessageId}")
                }
                case "Notification" => {
                  val notification = getObjectMessage[Notification](bodyStr)
                  logger.debug(s"Received ${snsHeaderType} with messageId: ${notification.get.MessageId}")
                  createUser(notification.get.Message)
                    .map(greeting => {
                      executePostGreeting(greeting = greeting.greeting)
                    })
                }
                case _ => {
                  logger.debug(s"Got a ${snsHeaderType}: ${bodyStr}")
                }
              }
              complete((StatusCodes.Created))
            }
          }
        }
    } ~ path("health") {
      get {
        complete("healthy")
      }
    }
}

trait SnsSubscriber {
  val logger = LoggerFactory.getLogger("SnsSubscriber")

  /**
    * Your code should read the HTTP headers of the HTTP POST requests that Amazon SNS sends to your endpoint.
    * Your code should look for the header field x-amz-sns-message-type, which tells you the type of message that Amazon SNS has sent to you.
    * By looking at the header, you can determine the message type without having to parse the body of the HTTP request.
    * There are two types that you need to handle: SubscriptionConfirmation and Notification.
    * The UnsubscribeConfirmation message is used only when the subscription is deleted from the topic.
    */

  def getObjectMessage[T](tStr: String)(implicit decoder: Decoder[T]): Option[T] = {
    parse(tStr) match {
      case Left(failure) => {
        logger.error(
          s"Invalid JSON :( with failure: ${failure.message}"
        )
        None
      }
      case Right(json) =>
        json.as[T] match {
          case Right(value) => {
            Some(value)
          }
          case Left(err) => {
            logger.error(s"Error: ${err.message}")
            None
          }
        }
    }
  }
  sealed trait SnsMessage
  object SubscriptionConfirmation {
    implicit val subscriptionConfirmation: Decoder[SubscriptionConfirmation] =
      deriveDecoder[SubscriptionConfirmation]
  }
  final case class SubscriptionConfirmation(
      MessageId: String /* potentially add more fields according to your needs, the model is in the tests and: https://docs.aws.amazon.com/sns/latest/dg/SendMessageToHttp.prepare.html */
  ) extends SnsMessage

  object Notification {
    implicit val notificationDecoder: Decoder[Notification] = deriveDecoder[Notification]
  }
  final case class Notification(
      MessageId: String,
      TopicArn: String,
      Subject: String,
      Message: NewJoiner,
      Timestamp: String,
      Signature: String,
      UnsubscribeURL: String
  ) extends SnsMessage
  final case object UnsubscribeConfirmation extends SnsMessage

}
