package me.greeter.challenge

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import me.greeter.challenge.models.{Greeting, NewJoiner}

import scala.collection.immutable

final case class Users(users: immutable.Seq[NewJoiner])

object NewJoinersRegistry {

  //introducing the actor's protocol
  sealed trait Command
  final case class GetNewJoiners(replyTo: ActorRef[Users])                            extends Command
  final case class NewUserJoined(user: NewJoiner, replyTo: ActorRef[ActionPerformed]) extends Command
  final case class ActionPerformed(greeting: Greeting)

  def apply(): Behavior[Command] = registry(Set.empty) //use its mailbox to keep a safe state with no vars or other ugly stuff

  private def registry(users: Set[NewJoiner]): Behavior[Command] =
    Behaviors.receiveMessage {
      case GetNewJoiners(replyTo) =>
        replyTo ! Users(users.toSeq)
        Behaviors.same
      case NewUserJoined(user, replyTo) =>
        val greeting = greetUser(currGreetedUser = user, buddies = users)
        replyTo ! ActionPerformed(greeting)
        registry(users.takeRight(5) + user) //take the most recent 5 users so we don't run out of memory
    }

  private def greetUser(currGreetedUser: NewJoiner, buddies: Set[NewJoiner]): Greeting = {
    val recentUserIds = buddies.takeRight(3).toArray.map(_.id) //too lazy to configure it

    val usersStr = if (buddies.size >= 2) {
      buddies.slice(0, buddies.size - 1).map(_.name).mkString(", ") + s" and ${buddies.last.name}"
    } else buddies.head.name
    val message =
      s"Hi ${currGreetedUser.name}, welcome to komoot. ${usersStr} also joined recently"
    Greeting("greeter@komoot.com", currGreetedUser.id, message, recentUserIds)
  }
}
