package me.greeter.challenge

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpRequest}
import akka.stream.Materializer
import com.typesafe.config.ConfigFactory
import io.circe.syntax.EncoderOps
import me.greeter.challenge.models.Greeting

import scala.concurrent.{ExecutionContext, Future}

trait PostGreeter {

  def postGreeting(path: String, greeting: Greeting): HttpRequest = {
    import akka.http.scaladsl.client.RequestBuilding.Post
    Post(path, HttpEntity(ContentTypes.`application/json`, greeting.asJson.toString()))
  }

  def executePostGreeting(
      path: String = s"${ConfigFactory.load().getString("greeterEndpoint")}",
      greeting: Greeting
  )(implicit
      system: ActorSystem[_],
      materializer: Materializer,
      ec: ExecutionContext
  ): Future[Boolean] = {
    val http = Http(system)
    val req  = postGreeting(path, greeting)
    http.singleRequest(req).map(_.status.isSuccess())

  }
}
