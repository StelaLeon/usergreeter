package me.greeter.challenge

import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives.{complete, extractRequestEntity}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import me.greeter.challenge.models.Greeting
import org.scalatest.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration.{Duration, DurationInt}

class PostgreeterSpecs extends AnyWordSpec with Matchers with ScalatestRouteTest with PostGreeter {

  val greeting = Greeting(
    sender = "your@email.com",
    receiver = 4,
    message = "Hi there !",
    recent_user_ids = Array(5, 6)
  )

  val route = extractRequestEntity { entity => complete(entity) }

  "Routes" should {
    "return no articles in the beginning" in {
      implicit val timeout: Duration = 3.seconds
      postGreeting("/notification", greeting) ~> route ~> check {
        println(s"responseEntity: ${response}")
        status shouldBe StatusCodes.OK
        contentType shouldBe ContentTypes.`application/json`
        //@todo verify body
      }
    }
  }
}
