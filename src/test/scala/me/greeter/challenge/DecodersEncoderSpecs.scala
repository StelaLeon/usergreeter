package me.greeter.challenge

import java.time.LocalDateTime

import io.circe.syntax.EncoderOps
import me.greeter.challenge.models.Greeting
import org.scalatest.Matchers
import org.scalatest.flatspec.AnyFlatSpec

class DecodersEncoderSpecs extends AnyFlatSpec
  with Matchers{
  "Decoders" should "decode perfectly fine" in {
    val jsonString =
      """
        |{
        |"name": "Marcus",
        |"id": 1589278470,
        |"created_at": "2020-05-12T16:11:54.000"
        |}
        |""".stripMargin
    import me.greeter.challenge.models.NewJoiner._

    val newJoiner = getNewJoiner(jsonString).get
    assert(newJoiner.name == "Marcus")
    assert (newJoiner.id == 1589278470)
    assert(newJoiner.createdAt === LocalDateTime.parse("2020-05-12T16:11:54")) //@todo maybe not?!

  }

  "Encoder" should "encode a case class" in{
    val encodedCaseCl = new Greeting(
      sender = "your@email.com",
      receiver = 4,
      message = "Hi there !",
      recent_user_ids = Array(5,6)
    )

    val jsobj = encodedCaseCl.asJson
    assert(jsobj.\\("sender").map(_.as[String].toOption.get).head == "your@email.com")
    assert(jsobj.\\("receiver").map(i => Integer.parseInt(i.toString())) == List(4))
    assert(jsobj.\\("message").map(_.as[String].toOption.get).head == "Hi there !") //@todo there's a better way
    assert(jsobj.\\("recent_user_ids").head.asArray.get.map(i => Integer.parseInt(i.toString())) == List(5,6))


  }
}

