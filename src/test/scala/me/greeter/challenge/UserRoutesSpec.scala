package me.greeter.challenge

import java.time.LocalDateTime

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.Materializer
import me.greeter.challenge.models.{Greeting, NewJoiner}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.{ExecutionContext, Future}

class UserRoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest {
  lazy val testKit = ActorTestKit()

  implicit def typedSystem = testKit.system

  override def createActorSystem(): akka.actor.ActorSystem =
    testKit.system.classicSystem

  val userRegistry = testKit.spawn(NewJoinersRegistry())

  lazy val routes = new ServiceRoutes(userRegistry) with PostGreeter {
    override def executePostGreeting(
        path: String = "localtest",
        greeting: Greeting
    )(implicit
        system: ActorSystem[_],
        materializer: Materializer,
        ec: ExecutionContext
    ): Future[Boolean] = {
      Future.successful(true)
    }
  }.userRoutes

  "ServiceRoutes" should {
    "return no users if no present (GET /health)" in {
      val request = HttpRequest(uri = "/health")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)

        entityAs[String] should ===("""healthy""")
      }
    }

    "be able to notify that a new user joined (POST notify)" in {
      val user = NewJoiner("Kapi", 42, LocalDateTime.parse("2020-05-12T16:11:54"))

      val userStr = """{
                      |  "name": "Johnny!!",
                      |  "id": 5,
                      |  "created_at": "2020-05-12T16:11:54"
                      |}""".stripMargin

      val request = HttpRequest(HttpMethods.POST, uri = "/notify", entity = userStr)

      request ~> RawHeader("x-amz-sns-message-type", "bla") ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
    }

    "receive a subscribtion just fine and parse it" in {
      val subscriptionConfirmation =
        """{
                                       |  "Type" : "SubscriptionConfirmation",
                                       |  "MessageId" : "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
                                       |  "Token" : "2336412f37f...",
                                       |  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
                                       |  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
                                       |  "SubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:123456789012:MyTopic&Token=2336412f37...",
                                       |  "Timestamp" : "2012-04-26T20:45:04.751Z",
                                       |  "SignatureVersion" : "1",
                                       |  "Signature" : "EXAMPLEpH+...",
                                       |  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
                                       |}""".stripMargin

      val request = HttpRequest(HttpMethods.POST, uri = "/notify", entity = subscriptionConfirmation)

      request ~> RawHeader("x-amz-sns-message-type", "SubscriptionConfirmation") ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
    }

    "receive a notification just fine and parse it" in {
      val newJoinerStr =
        """
          |{
          |"name": "Marcus",
          |"id": 1589278470,
          |"created_at": "2020-05-12T16:11:54.000"
          |}
          |""".stripMargin
      val notificationStr =
        s"""
          |{
          |  "Type" : "Notification",
          |  "MessageId" : "22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324",
          |  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
          |  "Subject" : "My First Message",
          |  "Message" : ${newJoinerStr},
          |  "Timestamp" : "2012-05-02T00:54:06.655Z",
          |  "SignatureVersion" : "1",
          |  "Signature" : "EXAMPLEw6JRN...",
          |  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
          |  "UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:c9135db0-26c4-47ec-8998-413945fb5a96"
          |}
          |""".stripMargin

      val request = HttpRequest(HttpMethods.POST, uri = "/notify", entity = notificationStr)

      request ~> RawHeader("x-amz-sns-message-type", "Notification") ~> routes ~> check {
        status should ===(StatusCodes.Created)
      }
    }
  }

}
