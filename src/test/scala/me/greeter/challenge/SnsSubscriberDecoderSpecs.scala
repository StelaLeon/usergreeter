package me.greeter.challenge

import org.scalatest.Matchers
import org.scalatest.flatspec.AnyFlatSpec

class SnsSubscriberDecoderSpecs extends AnyFlatSpec with Matchers  with SnsSubscriber {

  "SubscriptionConfirmation decoders" should "decode SubscriptionConfirmation messages perfectly fine" in {
    val subscriptionConfirmation = """{
                                     |  "Type" : "SubscriptionConfirmation",
                                     |  "MessageId" : "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
                                     |  "Token" : "2336412f37f...",
                                     |  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
                                     |  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
                                     |  "SubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:123456789012:MyTopic&Token=2336412f37...",
                                     |  "Timestamp" : "2012-04-26T20:45:04.751Z",
                                     |  "SignatureVersion" : "1",
                                     |  "Signature" : "EXAMPLEpH+...",
                                     |  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
                                     |}""".stripMargin

    val subscrConfirm = getObjectMessage[SubscriptionConfirmation](subscriptionConfirmation).get
    assert(subscrConfirm.MessageId.startsWith("165545c9"))
  }


  "SubscriptionConfirmation decoders" should "decode Notification messages perfectly fine" in {
    val newJoinerStr =
      """
        |{
        |"name": "Marcus",
        |"id": 1589278470,
        |"created_at": "2020-05-12T16:11:54.000"
        |}
        |""".stripMargin

    val notificationStr = s"""
                            |{
                            |  "Type" : "Notification",
                            |  "MessageId" : "22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324",
                            |  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
                            |  "Subject" : "My First Message",
                            |  "Message" : ${newJoinerStr},
                            |  "Timestamp" : "2012-05-02T00:54:06.655Z",
                            |  "SignatureVersion" : "1",
                            |  "Signature" : "EXAMPLEw6JRN...",
                            |  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
                            |  "UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:c9135db0-26c4-47ec-8998-413945fb5a96"
                            |}
                            |""".stripMargin

    val subscrConfirm = getObjectMessage[Notification](notificationStr).get
    assert(subscrConfirm.MessageId.startsWith("22b80b92"))
  }
}
