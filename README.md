## Setup
    tech stack: sbt/scala/akka
                akka-http
                circe ser/deser 
Why circe? 
    I used circe due to some tools like Show typeclass that I get out of the box. I like their more generic and easy to work with result type: Either[Failure, T] that makes it more intuitive than Spray's Result.
    I am interested to dive deeper into more large scale usage of Shapeless
    Circe sees the JSON as a protocol and nothing else, so it uses a lot of case classes.
    NONOs I did: I didn't plug in the circe into akka-http so this breaks a bit the error handling (the lib connector that i never used seemed a bit dodgy at first sight).

Running tests: sbt test

Set up your aws login credentials via ~/.aws/credentials file
Configure your aws cli via aws configure (e.g. aws ec2 describe-images should show you the running instances, be aware of the region missmatch) or just run it via docker :
    
    docker run --rm -it amazon/aws-cli:2.0.4 -v ~/.aws:/root/.aws amazon/aws-cli <entry command>
    aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin <my_aws_id>.dkr.ecr.us-east-2.amazonaws.com/<servicename>
    

Build a local docker image with sbt(docker plugin available that lets you expose ports and set up other vars through build.sbt) and push it to AWS registry: 

    sbt docker:publishLocal    << this will produce a docker image locally that you can next tag and push in ECR
    stela@Stela:~$ docker tag new-user-greeter:1.0 <my_aws_id>.dkr.ecr.us-east-2.amazonaws.com/greeterdocker:latest
    stela@Stela:~$ docker push <my_aws_id>.dkr.ecr.us-east-2.amazonaws.com/greeterdocker:latest
    
WIP: make an automatic deployment script that can update the aws deployment automatically.

Once ran the service's health can be accessible through GET /health endpoint

The dockerized service is deployed on ECS-cluster on an EC2 instance with CloudWatch logs() and health endpoint set up accordingly. 
    
## Installation
    aws-cli : curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    docker demon
    
    docker-compose for local testing  << @todo this is still work in progresss, the code is in KafkaStreamPipeline
    
    //https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html 



##Requirements
'
Steps:
####1 Read events sent by an SNS topic: https://docs.aws.amazon.com/sns/latest/dg/sns-http-https-endpoint-as-subscriber.html 

   Subscribing to an SNS topic is done via: 
    -you can subscribe through a queue or an http endpoint as per: 
        
    //https://github.com/serverless/serverless/issues/3676
    stela@Stela:~$ aws sns subscribe --region eu-west-1  --topic-arn arn:aws:sns:eu-west-1:<someid>:challenge-backend-signups    --protocol http    --notification-endpoint http://ec2-<external ip>.us-east-2.compute.amazonaws.com/notify
    {
          "SubscriptionArn": "pending confirmation"
    }
The zone is required in the command above because the cluster i deployed is in a different zone than the topic which is problematic due to the way amazon separates the zones.
    
Types of messages(the entire protocol's model is described in SnsSubscriber trait): 
   
    - SubscriptionConfirmation
    - Notification: 
        in the body of the Message field there's:
             {
             "name": "Marcus",
             "id": 1589278470,
             "created_at": "2020-05-12T16:11:54.000"
             }
    - UnsubscribeConfirmation
    
####2 Gather info about the users.

At the moment due to lack of an efficient memory cleaning mechanism the service is only keeping the last 5 elements, which makes sure to always change the users and not return always the same ones. 

-> The service is gathering the new joiners via the NewJoinerRegistry actor's mailbox, since it doesn't use a mutable data structure to keep its state, rather actor's 'behavior' to keep the last users, this way it's safer and secure that the state-var doesn't escape to terrorize the service stability. 
-> using GET /notify you can retrieve the users that are held in memory of the service. 
-> The SNS-subscription end point is listening on POST /notify with 'x-amz-sns-message-type' header set up for each message.


####3 POST notification to subscription service  configured in main/resources/application.conf (resources/test/application.conf for tests)
 The PostGreeter trait is describing a simple http post to the notification service. 
 Once the notification endpoint is recording an event it will forward it to the NewJoinerRegistry that records it and parses it answering back with a Greeting object that gets posted to the notification service.
 
        {
             "sender": "{your@mail.com}",
             "receiver": 1589278470,
             "message": "Hi Marcus, welcome to komoot. Lise, Anna and Stephen also joined recently.",
             "recent_user_ids": [627362498, 1093883245, 304390273]
        }
 
 
 ####useful docs:
    https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-agent-update.html
    https://docs.aws.amazon.com/AmazonECS/latest/developerguide/using_awslogs.html#specify-log-config
    